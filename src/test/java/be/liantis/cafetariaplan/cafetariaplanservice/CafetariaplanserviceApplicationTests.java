package be.liantis.cafetariaplan.cafetariaplanservice;

import org.junit.jupiter.api.Test;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.flyway.FlywayAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
@EnableAutoConfiguration(exclude = FlywayAutoConfiguration.class)
public class CafetariaplanserviceApplicationTests {

    @Test
    public void contextLoads() {
    }
}
