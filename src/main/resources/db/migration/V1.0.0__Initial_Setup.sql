--create manager
create table MANAGER
(
    id         varchar2(36)  not null,
    first_name varchar2(500) not null,
    last_name  varchar2(500) not null,
    email      varchar2(500) not null,
    language   varchar2(2)   null,
    CONSTRAINT MANAGER_EMAIL UNIQUE (EMAIL)
);

alter table MANAGER
    add constraint MANAGER_PK primary key (ID);

grant select, insert, update, delete on MANAGER to offerte_service;
grant select, insert, update, delete on MANAGER to offerte;

